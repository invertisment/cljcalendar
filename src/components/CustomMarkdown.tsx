import ReactMarkdown from "react-markdown";

export function renderMarkdown(md: string) {
  return <ReactMarkdown
    components={{
      h1: 'h4',
      h2: 'h5',
      h3: 'h6',
      h4: 'h6',
      h5: 'h6',
      p: ({ node, ...props }) => <p {...props} className="p-with-newlines"></p>,
      a: ({ node, ...props }) => <a {...props} className="p-with-newlines" target="_blank"></a>,
    }} >
    {md}
  </ReactMarkdown>
}
