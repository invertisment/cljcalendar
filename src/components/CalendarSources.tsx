import React, { useContext, useState, ReactNode } from "react";
import { calendars, EventSource } from "../calendars";

const STORAGE_KEY = "calendar-ids"

export const defaultEnabled = new Set(["clojure"])

function loadFromStorage() {
  try {
    const input = window.localStorage.getItem(STORAGE_KEY)
    if (!input) {
      return defaultEnabled
    }
    const calIds = new Set<string>(JSON.parse(input))
    if (calIds.size == 0) {
      return defaultEnabled
    }
    return calIds
  } catch (_ignored) {
    return defaultEnabled
  }
}

function mkCtxValue(enabledIds: Set<string>, setter: (enabledIds: string[]) => void) {
  return {
    enabledIds: enabledIds,
    calInfos: calendars.filter(cal => enabledIds.has(cal.key)),
    setEnabledCalIds: setter,
  }
}

const SourceContext = React.createContext(mkCtxValue(
  defaultEnabled,
  (_ids: string[]) => {
    throw Error("Can't update enabled calendars; DOM is loaded improperly.")
  }
));

export function CalendarSourceProvider(props: { children: ReactNode }) {
  const [enabledCalIds, setEnabledCalIds] = useState<Set<string>>(loadFromStorage());
  return (
    <SourceContext.Provider value={mkCtxValue(
      enabledCalIds,
      (ids: string[]) => {
        window.localStorage.setItem(STORAGE_KEY, JSON.stringify(ids))
        setEnabledCalIds(new Set(ids))
      }
    )}>
      {props.children}
    </SourceContext.Provider>
  )
}

export function useEnabledCalendars(): EventSource[] {
  return useContext(SourceContext).calInfos.map(cal => cal.eventSource);
}

export function useEnabledCalendarCtx() {
  return useContext(SourceContext)
}
