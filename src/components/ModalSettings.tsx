import { Dispatch, Fragment, SetStateAction } from 'react';
import CloseableModal from './CloseableModal';
import './ModalSettings.scss';
import { calendars, CalInfo } from '../calendars'
import { useEnabledCalendarCtx } from './CalendarSources';
import { renderMarkdown } from './CustomMarkdown';

function link(title: string, href: string) {
  return <a href={href} target="_blank">{title}</a>
}

export default function ModalSettings(props: {
  visibilityCtrl: [boolean, Dispatch<SetStateAction<boolean>>]
}) {
  const { enabledIds, setEnabledCalIds } = useEnabledCalendarCtx()
  return (
    <CloseableModal
      visibilityCtrl={props.visibilityCtrl}
      title="Front-end for Clojure events calendar feed" >
      <h3>Select your preferred groups</h3>
      {calendars.map((calendar: CalInfo) => {
        return <Fragment key={calendar.key}>
          <div className="justify-sb div-border">
            <span className="bold">{calendar.title}</span>
            <button
              onClick={() => {
                const newLi = enabledIds.has(calendar.key)
                  ? Array.from(enabledIds).filter(
                    item => item != calendar.key
                  )
                  : [...Array.from(enabledIds), calendar.key]
                setEnabledCalIds(newLi)
              }}>
              {enabledIds.has(calendar.key)
                ? "Disable"
                : "Enable"}
            </button>
          </div>
          <div className="ident-left">
            {renderMarkdown(calendar.desc)}
          </div>
        </Fragment>
      })}
      <hr>
      </hr>
      <h3>About</h3>
      <div className="ident-left">
        Web UI ({link(
          "Source code",
          "https://gitlab.com/invertisment/cljcalendar/"
        )}): {link("Martynas Maciulevičius", "https://mmaciul.lt")}
        <p>
          If you want to add your own calendar to this UI then ping me on&nbsp;
          {link("Slack", "https://clojurians.slack.com/team/U028ART884X")}.
        </p>
      </div>

    </CloseableModal >
  );
}
