
export interface EventSource {
  url: string,
  format: string,
}

export interface CalInfo {
  key: string
  title: string
  location: string,
  country: string,
  eventSource: EventSource,
  desc: string
}

export const calendars: CalInfo[] = [
  {
    key: "clojure",
    title: "Clojurians Zulip events",
    eventSource: {
      url: 'https://www.clojurians-zulip.org/feeds/events.ics',
      format: "ics",
    },
    desc: `
To add new events visit \
[Clojure events feed README](https://clojurians.zulipchat.com/#narrow/stream/262224-events/topic/README) \
and \
[Announce an Event](https://gitlab.com/clojurians-zulip/feeds/-/tree/master#announce-an-event). \n
Maintainer: [Gert Goet](https://github.com/eval)`,
    country: "Any",
    location: "Any",
  },

  {
    key: "clojure-madison",
    title: "Madison Clojure",
    country: "United States of America",
    location: "Madison, Wisconsin",
    eventSource: {
      url: 'https://madclj.com/events.ics',
      format: "ics"
    },
    desc: `
Webpage: [Madison Clojure](https://madclj.com/) \n
Maintainer: [Ambrose Bonnaire-Sergeant](https://ambrosebs.com/)
`,
  },
]
