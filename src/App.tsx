import './App.css';
import ICal from './components/ICal';

function App() {
  return (
    <div className="App" id="app">
      <ICal />
    </div>
  );
}

export default App;
